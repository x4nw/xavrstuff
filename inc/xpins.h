// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#ifndef XPINS_H
#define XPINS_H
#define INSIDE_XPINS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// Macros for pin definition and manipulation.

// Pins are defined as a bitfield with several useful components encoded,
// making it very easy to quickly extract information from them:
//
// 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
// |^^^^^^^^^^ |^^^^^^^^^^ |^^^^^^^^^^^^^^^^^^^^^^
// |           |           |____________________________ bit mask
// |           |
// |           |________________________________________ port number
// |
// |____________________________________________________ pin number
//
// Decoding these will generally happen at compile time, but even if you pass
// one as a function argument and it can't get optimized, they're very fast to
// access.

typedef uint16_t xpin_t;

// Define a pin from port and pin numbers. Used internally to this header, not
// really recommended for your own use...
#define XDEFPIN(nport_, npin_) \
	(xpin_t)(((nport_) << 8u) | ((npin_) << 12u) | (1u << (npin_)))

// Extract the port number from a pin def
#define XPIN_NPORT(pin_) (((pin_) & 0x0F00u) >> 8u)
// Extract the pin number from a pin def
#define XPIN_NPIN(pin_) (((pin_) & 0xF000u) >> 12u)
// Extract the pin mask from a pin def
#define XPIN_MASK(pin_) ((pin_) & 0x00FFu)

#define XPA0 XDEFPIN(0u, 0u)
#define XPA1 XDEFPIN(0u, 1u)
#define XPA2 XDEFPIN(0u, 2u)
#define XPA3 XDEFPIN(0u, 3u)
#define XPA4 XDEFPIN(0u, 4u)
#define XPA5 XDEFPIN(0u, 5u)
#define XPA6 XDEFPIN(0u, 6u)
#define XPA7 XDEFPIN(0u, 7u)
#define XPB0 XDEFPIN(1u, 0u)
#define XPB1 XDEFPIN(1u, 1u)
#define XPB2 XDEFPIN(1u, 2u)
#define XPB3 XDEFPIN(1u, 3u)
#define XPB4 XDEFPIN(1u, 4u)
#define XPB5 XDEFPIN(1u, 5u)
#define XPB6 XDEFPIN(1u, 6u)
#define XPB7 XDEFPIN(1u, 7u)
#define XPC0 XDEFPIN(2u, 0u)
#define XPC1 XDEFPIN(2u, 1u)
#define XPC2 XDEFPIN(2u, 2u)
#define XPC3 XDEFPIN(2u, 3u)
#define XPC4 XDEFPIN(2u, 4u)
#define XPC5 XDEFPIN(2u, 5u)
#define XPC6 XDEFPIN(2u, 6u)
#define XPC7 XDEFPIN(2u, 7u)
#define XPD0 XDEFPIN(3u, 0u)
#define XPD1 XDEFPIN(3u, 1u)
#define XPD2 XDEFPIN(3u, 2u)
#define XPD3 XDEFPIN(3u, 3u)
#define XPD4 XDEFPIN(3u, 4u)
#define XPD5 XDEFPIN(3u, 5u)
#define XPD6 XDEFPIN(3u, 6u)
#define XPD7 XDEFPIN(3u, 7u)

// Future TODO: detect the correct header and use it
#include <xpins_xmtiny.h>

#ifdef __cplusplus
}
#endif

#undef INSIDE_XPINS_H
#endif // !defined(XPINS_H)

// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#ifndef INSIDE_XPINS_H
# error "xpins_xmtiny.h should be included by xpins.h only"
#endif

// Peripheral decoders! These can return peripheral-specific info about pins.

// Get the MUXPOS flag for the ADC input on this pin. If there is no ADC
// input on this pin, gives ADC_MUXPOS_GND_gc
#define XPIN_ADC_MUXPOS(pin_) ( \
	(pin_) == XPA0 ? ADC_MUXPOS_AIN0_gc : \
	(pin_) == XPA1 ? ADC_MUXPOS_AIN1_gc : \
	(pin_) == XPA2 ? ADC_MUXPOS_AIN2_gc : \
	(pin_) == XPA3 ? ADC_MUXPOS_AIN3_gc : \
	(pin_) == XPA4 ? ADC_MUXPOS_AIN4_gc : \
	(pin_) == XPA5 ? ADC_MUXPOS_AIN5_gc : \
	(pin_) == XPA6 ? ADC_MUXPOS_AIN6_gc : \
	(pin_) == XPA7 ? ADC_MUXPOS_AIN7_gc : \
	(pin_) == XPB1 ? ADC_MUXPOS_AIN10_gc : \
	(pin_) == XPB0 ? ADC_MUXPOS_AIN11_gc : \
	ADC_MUXPOS_GND_gc)

#define _XPIN_ALT_CFG(cond_, reg_, bit_) do { \
	if (cond_) PORTMUX.reg_ |= (bit_); \
	else PORTMUX.reg_ &= ~(bit_); } while (0)

// Check whether a given USART pin is in an alternate position
#define XPIN_USART_IS_ALT(pin_) (XPIN_NPORT(pin_) == 0)
// Configure the USART alternate setting based on a pin
#define XPIN_USART_ALT_CFG(pin_) \
	_XPIN_ALT_CFG(XPIN_USART_IS_ALT(pin_), CTRLB, PORTMUX_USART0_bm)

// Check whether a given TWI pin is in an alternate position
#define XPIN_TWI_IS_ALT(pin_) (XPIN_NPORT(pin_) == 0)
// Configure the TWI alternate setting based on a pin
#define XPIN_TWI_ALT_CFG(pin_) \
	_XPIN_ALT_CFG(XPIN_TWI_IS_ALT(pin_), CTRLB, PORTMUX_TWI0_bm)

// Get the timer CMP register (single mode) for a pin
#define XPIN_TCA_SGL_CMPn(pin_) ( \
	(pin_) == XPB0 ? &TCA0.SINGLE.CMP0 : \
	(pin_) == XPB3 ? &TCA0.SINGLE.CMP0 : \
	(pin_) == XPB1 ? &TCA0.SINGLE.CMP1 : \
	(pin_) == XPB2 ? &TCA0.SINGLE.CMP2 : \
	NULL)
// Get the timer CMP register (split mode) for a pin
#define XPIN_TCA_SPT_CMPn(pin_) ( \
	(pin_) == XPB0 ? &TCA0.SPLIT.LCMP0 : \
	(pin_) == XPB3 ? &TCA0.SPLIT.LCMP0 : \
	(pin_) == XPB1 ? &TCA0.SPLIT.LCMP1 : \
	(pin_) == XPB2 ? &TCA0.SPLIT.LCMP2 : \
	(pin_) == XPA3 ? &TCA0.SPLIT.HCMP0 : \
	(pin_) == XPA4 ? &TCA0.SPLIT.HCMP1 : \
	(pin_) == XPA5 ? &TCA0.SPLIT.HCMP2 : \
	NULL)
// Check whether a timer WOn pin is in an alternate position
#define XPIN_TCA_IS_ALT(pin_) ((pin_) == XPB3)
// Configure the TCA alternate setting based on a pin
#define XPIN_TCA_ALT_CFG(pin_) \
	_XPIN_ALT_CFG(XPIN_TCA_IS_ALT(pin_), CTRLC, PORTMUX_TCA0_bm)

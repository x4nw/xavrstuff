// © by xan. All rights reserved. See accompanying COPYING.md for more info.

// xpins implementation for the xmega-like attiny (and probably atmega) family.
// Meant to be included by xpins.h

#ifndef INSIDE_XPINS_H
# error "xpins_xmtiny.h should be included by xpins.h only"
#endif

#include <avr/io.h>

// Do any of these chips not have PORTA? If that's the case, I'll have to
// define versions of these that can pivot off whatever ports ARE defined. For
// now, this is fine.

#define XPIN_PORT(pin_) ( \
	(XPIN_NPORT(pin_) == 0) ? &PORTA : \
	(XPIN_NPORT(pin_) == 1) ? &PORTB : \
	NULL)

#define XPIN_VPORT(pin_) ( \
	(XPIN_NPORT(pin_) == 0) ? &VPORTA : \
	(XPIN_NPORT(pin_) == 1) ? &VPORTB : \
	NULL)

#define XPIN_PINCTRL(pin_) ((&XPIN_PORT(pin_)->PIN0CTRL)[XPIN_NPIN(pin_)])

// Set pin output 1
#define XPIN_OUT1(pin_) do { \
	XPIN_VPORT(pin_)->OUT |=  XPIN_MASK(pin_); } while (0)
// Set pin output 0
#define XPIN_OUT0(pin_) do { \
	XPIN_VPORT(pin_)->OUT &= ~XPIN_MASK(pin_); } while (0)
// Write pin output
#define XPIN_OUT(pin_, v_) do { \
	if (v_) XOUT_1(pin_); else XOUT_0(pin_); } while (0)

// Get pin input
#define XPIN_IN(pin_) (XPIN_VPORT(pin_)->IN & XPIN_MASK(pin_))

// Get pin interrupt state
#define XPIN_INT(pin_) (XPIN_VPORT(pin_)->INTFLAGS & XPIN_MASK(pin_))
// Clear pin interrupt state
#define XPIN_INTCLEAR(pin_) do { \
	if (__builtin_constant_p(pin_) \
	 && __builtin_popcount(XPIN_MASK(pin_)) == 1) \
		XPIN_VPORT(pin_)->INTFLAGS |= XPIN_MASK(pin_); \
	else \
		XPIN_VPORT(pin_)->INTFLAGS = XPIN_MASK(pin_); \
	} while (0)

// Set pin driven
#define XPIN_DRIVE(pin_) do { \
	XPIN_VPORT(pin_)->DIR |= XPIN_MASK(pin_); } while (0)

// Set pin undriven
#define XPIN_RELEASE(pin_) do { \
	XPIN_VPORT(pin_)->DIR &= ~XPIN_MASK(pin_); } while (0)

// Apply a pin configuration. cfg_ should be a bitwise OR of the flags below.
#define XPIN_CFG(pin_, cfg_) do { XPIN_PINCTRL(pin_) = (cfg_); } while (0)

// Pin configuration flags. These directly map to PINnCTRL flags here, but
// another chip's driver could decode them instead.
#define XDEFAULT  PORT_ISC_INTDISABLE_gc
#define XPULLUP   PORT_PULLUPEN_bm
#define XNOBUFFER PORT_ISC_INPUT_DISABLE_gc /* never combine with XINT* */
#define XINT_FALL PORT_ISC_FALLING_gc
#define XINT_RISE PORT_ISC_RISING_gc
#define XINT_BOTH PORT_ISC_BOTHEDGES_gc

#if defined(__AVR_ATtiny214__) || defined(__AVR_ATtiny414__) || defined(__AVR_ATtiny814__)
#include "xpins_xmtiny_tnx14.h"
#else
#warning "No peripheral decoders available for this microcontroller"
#endif

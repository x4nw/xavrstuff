# xavrstuff

xavrstuff (xan's avr stuff) is my collection of little build tools and code for
AVR microcontrollers. It's not meant to be a comprehensive library — some stuff
in here might be useful to you, but if I haven't needed it I probably haven't
written it.

**WARNING:** xavrstuff is *not* open source, though you may use it for yourself.
If you do so, please link to it (e.g. using a submodule reference) rather than
copying it. See [COPYING.md](COPYING.md) for more information.

## xpins

Currently supports: ATtiny214, ATtiny414, ATtiny814

xpins.h provides a simple and flexible interface for interacting with pins.
First, it provides a constant for each pin, named like XPA0, XPC3, etc. These
constants encode the port number and pin number in an integer (`xpin_t` which
is `uint16_t`) to allow pins to be referred to by name.

Then, a number of macros are provided to use these:

- All devices:
    - `XPIN_NPORT(pin)`: extract the port number (PORTA = 0, PORTB = 1, ...)
      from a pin constant.
    - `XPIN_NPIN(pin)`: extract the pin number (PA0 = 0, PA3 = 3, ...) from a
      pin constant.
    - `XPIN_MASK(pin)`: extract the pin bitmask (PA0 = 0x01, PA3 = 0x08, ...)
      from a pin constant.

- Most devices:
    - `XPIN_OUT1(pin)`: set the output state to 1
    - `XPIN_OUT0(pin)`: set the output state to 0
    - `XPIN_OUT(pin, v)`: set the output state to v
    - `XPIN_IN(pin)`: get the input state
    - `XPIN_INT(pin)`: get the interrupt state (1 = tripped)
    - `XPIN_INTCLEAR(pin)`: clear the interrupt state
    - `XPIN_DRIVE(pin)`: set pin driven
    - `XPIN_RELEASE(pin)`: set pin undriven
    - `XPIN_CFG(pin, cfg)`: set pin configuration, using a set of bitmask flags
        - `XDEFAULT`
        - `XPULLUP`
        - `XNOBUFFER`
        - `XINT_FALL`
        - `XINT_RISE`
        - `XINT_BOTH`

In addition, chip-specific macros are provided to decode peripheral information
from each pin. For example, the ATtinyX14 implementation provides:

- `XPIN_ADC_MUXPOS(pin)`: get the MUXPOS constant for an ADC pin
- `XPIN_TCA_SGL_CMPn(pin)`: get the TCAn.SINGLE.CMPn register for this pin
- `XPIN_TCA_SPT_CMPn(pin)`: get the TCAn.SPLIT.mCMPn register for this pin
- `XPIN_*_IS_ALT(pin)`: get whether the \* peripheral pin (USART, TWI, TCA, etc)
  is in an alternate position
- `XPIN_*_ALT_CFG(pin)`: set up the \* peripiheral's alternate position
  configuration according to this pin
